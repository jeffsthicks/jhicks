\documentclass{amsart}
\newcommand{\solution}{\noindent\textbf{Solution:}}
\usepackage[margin=1in]{geometry}
\usepackage{enumerate}
\usepackage{graphicx}
%\input{preamble}
\begin{document}
\section*{Quiz 4}
\section*{Name:\hspace{4cm}\;}
\subsection*{Problem 1} The following sheet of metal is submerged in a tank of water, so that the top of the sheet just barely sits below the water. Calculate the hydrostatic pressure on this metal sheet (you may use the letter $g$ in your calculations, instead of ``$9.8$-something'') and may assume that the density of water is $1000$kg/$\text m^3$
\[\includegraphics[scale=.15]{pressure}\]
\solution We use the formula for hydrostatic pressure. Setting the $z$ axis from the top going down, each slice of the metal sheet has an ``area'' of \[ 2-z\, dz\]
And so the total force on the sheet is
\begin{align*}
\int_0^1\rho g z  (2-z)\,dz=&1000kg/m^2 g \int_0^1 2z-z^2\, dz\\
=&1000kg/m^3 g (z^2-z^3/3)\bigg|_0^1\\
=&2000/3kg/m^3
\end{align*}
\subsection*{Problem 2}
Consider the function $P(x)=\frac{\pi}{2} \sin(\pi x)$, where $x$ is between $0$ and $1$. 
\begin{enumerate}[(a)]
\item Show that $P(x)$ is a probability distribution
\item Show that the mean, mode and median of $P(x)$ are the same.
\item Is it always true that the mean, mode and median or a probability distribution are the same? If so, provide a reason. If not, draw a counterexample. 
\end{enumerate}
\solution
\begin{enumerate}[(a)]
\item $\int_0^1P(x)dx=\int_0^1 \frac{\pi}{2}\sin(\pi x)dx=\frac{1}{2}-\cos(\pi x)\bigg|_0^1=\frac{2}{2}=1$
\item For the mode, we must find the maximum of the function. The derivative of $P(x)=\pi^2/2(\cos \pi x)$, which has a zero when $x=1/2$. This is a maximum of the function, so the mode of $P(x)$ is 1/2.\\
For the median, we try 1/2 and find that $\int_0^{1/2}\sin(\pi x)=-\cos(\pi x)\bigg|_0^{1/2}=\frac{1}{2}$, so this is the median.\\
For the mean of the function, we integrate
\begin{align*}\int_0^1 x P(x) dx =& \int_0^1 \frac{\pi}{2} x\sin(\pi x)\\
=&-x\cos (\pi_x)\bigg|_0^\frac{1}{2}-\int_0^1 \frac{-\pi}{2}\cos(\pi(x))dx\\
=&\frac{1}{2}
\end{align*}
which is the desired result.
\item No. Consider $2x$ on the interval $[0,1]$. Then the mode is 1, but the median is something different.



\end{enumerate}•
\newpage

\subsection*{Problem 3} Suppose that a sequence $a_n$ does not converge to $0$. Show that the sum $\sum_{n=1}^\infty a_n$ does not converge.\\


\textit{I've put three solutions here. You should be comfortable with the moral solution. The rigorous and ``short but difficult'' solution are if you are having difficulty with sequences and want some practice looking at them. The offset sections are short explanations for the more difficult sections of the proof.}\\


\solution  (Morally)  Let $s_m=\sum_{n=1}^m a_n$ be the partial sum. We say that $s_m$ converges if the terms in the sum get infinitely close to each other. What does that mean? It means that $s_m-s_{m-1}$ should got to $0$. But $s_m-s_{m-1}=a_m$, and $a_m$ does not go to zero. Therefore $s_m$ does not converge. \\


\solution (Rigorously) We will instead show that if the sum converges, then $a_n$ also converges. This will prove that if $a_n$ does not converge, then the sum cannot converge.
\begin{quote}{(Aside: You should check to make sure you know why this is true-- in logic, the statement ``If $A$ is true then $B$ is true'' is the same as ``If $B$ is not true, then $A$ is not true''. This can take a while to wrap your head around, but understanding this type of reasoning will make a lot of things in life easier.)}
\end{quote}

\noindent Suppose that $s_m$ converges to $S$. For us to show that $a_n$ converges to $0$, we must show that for every $\epsilon\geq 0$ that we pick, there exists an $N\geq 0$ so that for all $n\geq N$ we have that $|a_n-0|\leq \epsilon$.
\begin{quote} In other words, we need to show that for as close as we want our sequence to get to $0$ (that's $\epsilon$), there is a point in the sequence (that's $N$) where from there on out, the sequence remains that close to 0. 
\end{quote}
So let us pick an $\epsilon$. By assumption, $s_m$ converges to $S$. Then by the definition of convergence, for any $\epsilon_1 \geq 0$ we might pick, there exists an $N$ so that for all $n\geq N$ we have that $|s_n-S|<\epsilon_1$. In particular, we could pick $\epsilon_1$ to be $\frac{\epsilon}{2}$
\begin{quote} Tricky! There are two different $\epsilon$ here. One is the one that we picked to show that $a_n$ converges. The second we get for free because we are supposing that $s_m$ converges. 
\end{quote}
So now we have that for all $n\geq N$, $|s_n-S|\leq \frac{\epsilon}{2}$ This means that for all $n$, $|s_n-s_{n+1}|\leq 2\frac{ \epsilon}{2}=\epsilon$ 
\begin{quote} Aside: You should check to make sure you know why this is true. It is called the ``triangle inequality'')
\end{quote}
This means that for all $n\geq N$ $|a_n|=|s_n-s_{n-1}|\leq \epsilon$. We have now shown that $a_n$ converges to $0$. Why is this? Because for any $\epsilon$ we picked, we found an $N$ so that for all $n\geq N$, $|a_n|\leq \epsilon$. \\

\solution  (Shortly, but difficultly) If $s_n$ were to converge to $S$, then for every $\epsilon \geq0$ there must exist $N$ so that for all $n\geq N$, $s_n-S$\leq 0$. This means that $|s_n-s_{n+1}|\leq 2\epsilon$. As $a_n$ does not converge to zero, there exists $\epsilon\geq 0$ such that for infinitely many $n$ we have that $|a_n|/2> \epsilon$. Then 
$|s_n-s_{n+1}|=|a_n|> 2\epsilon$. Which means that $s_n$ cannot converge.
\end{document}
